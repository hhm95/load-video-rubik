import 'package:flutter/material.dart';

class ItemMessageTemplate extends StatelessWidget {

  final Color colorBackground;
  final Color colorBorder;
  final String contentMessage;

  final Function(String, int) onClickItem;   //callback function

  //contructor
  ItemMessageTemplate({this.colorBackground, this.colorBorder, this.contentMessage, this.onClickItem});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          height: 140,
          decoration: BoxDecoration(
              color: colorBackground,
              borderRadius: BorderRadius.circular(50/2),
              border: Border.all(color: colorBorder, width: 2)
          ),
          child: FlatButton(
              onPressed: (){
                onClickItem(contentMessage, 123);
              },
              child: Center(child: Text(contentMessage, style: TextStyle(color: Colors.white),)))),
    );
  }
}
