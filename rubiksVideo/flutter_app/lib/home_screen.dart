import 'package:flutter/material.dart';
import 'package:flutter_app/girl_screen.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var _currentIndex=0;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: _buildAppBar(),
      body: IndexedStack(
        index: _currentIndex,
        children: [
          // GirlScreen(),
          // BoyScreen()
          Container(color: Colors.white,),
          Container(color: Colors.white,),
          Container(color: Colors.white,),
        ],
      ),
      bottomNavigationBar: _buiBottomNavigation()
    );
  }

  _buildAppBar() {
    return AppBar(
      title: IndexedStack(
        index: _currentIndex,
        children: [
          // GirlScreen(),
          // BoyScreen()
          Center(child: Text('Assest', style: TextStyle(fontSize: 16, color: Colors.white))),
          Center(child: Text('Local', style: TextStyle(fontSize: 16, color: Colors.white))),
          Center(child: Text('Internet', style: TextStyle(fontSize: 16, color: Colors.white))),

        ],
      ),
    );
  }

  _buildBody() {
    return Text("123");
  }

  _buiBottomNavigation() {
    return BottomNavigationBar(
        backgroundColor: Colors.blueAccent,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black,
        currentIndex: _currentIndex,
        onTap: (index) {
      setState(() {
        _currentIndex = index;
      });
    },
    items: [
      BottomNavigationBarItem(icon: Icon(Icons.web_asset_outlined), label: 'Asset'),
      BottomNavigationBarItem(icon: Icon(Icons.folder_outlined), label: 'Local'),
      BottomNavigationBarItem(icon: Icon(Icons.search_outlined), label: 'Internet'),
    ]
    );
  }
}
