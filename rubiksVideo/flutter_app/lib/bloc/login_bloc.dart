import 'package:rxdart/rxdart.dart';


class LoginBloc {
  final _userController = new BehaviorSubject<String>();

  Stream<String> get userStream => _userController.stream;

  bool isValidInfo(String username, String password, String link) {
    bool isValid = true;
    if (username.isEmpty) {

      _userController.add('Nhập tên tài khoản');
      isValid = false;
    }else {
        _userController.add(null);
    }

    return isValid;

  }

  void dispose() {
    _userController.close();
  }
}