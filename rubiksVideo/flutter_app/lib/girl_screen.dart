import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/bloc/login_bloc.dart';
import 'package:flutter_app/widget.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class GirlScreen extends StatefulWidget {
  @override
  _GirlScreenState createState() => _GirlScreenState();
}

class _GirlScreenState extends State<GirlScreen> {

  LoginBloc bloc = new LoginBloc();
  TextEditingController _userController = new TextEditingController();
  FlutterLocalNotificationsPlugin notificationsPlugin;

  final String text1  = "😋 Love U 1";
  final String text2  = "😋 Love U 2";
  final String text3  = "😋 Love U 3";
  final String text4  = "😋 Love U 4";
  final String text5  = "😋 Love U 5";
  final String text6  = "😋 Love U 6";

  var list = ["😋 Love U 1", "😋 Love U 2", "😋 Love U 3", "😋 Love U 4", "😋 Love U 5", "😋 Love U 6", "😋 Love U 7"];

  @override
  void initState() {
    FirebaseFirestore.instance
        .collection("notification")
//        .where("address.country", isEqualTo: "USA")
        .snapshots()
        .listen((result) {
          result.docs.forEach((result) {
          print(result.data());
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,

            child: Column(
              mainAxisSize: MainAxisSize.max,
              //mainAxisAlignment: MainAxisAlignment.start,
              //crossAxisAlignment: CrossAxisAlignment.start,
                children:[
                  Padding(
                      padding: const EdgeInsets.fromLTRB(40, 10, 40, 5),
                      child: StreamBuilder(
                          stream: bloc.userStream,
                          builder: (context, AsyncSnapshot<String> snapshot) {
                            String error;
                            if (snapshot.hasData) {
                              error = snapshot.data;
                            }
                            return TextField(
                              style: TextStyle(fontSize: 18, color: Colors.black),
                              controller: _userController,
                              decoration: InputDecoration(
                                  labelText: "Nhập mã số Boy",
                                  errorText: error,
                                  labelStyle: TextStyle(
                                      color: Color(0xff888888), fontSize: 15)),
                            );
                          })),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Center(
                      child: Container(
                        width: 250,
                        child: RaisedButton(
                          color: Colors.blue[500],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(20))),
                          onPressed: onGetBoyClicked(),
                          child: Text(
                            "Triệu hồi Boy 🤞",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(left: 16, right: 16),
                    child: GridView.builder(
                      itemCount: list.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 16,
                        mainAxisSpacing: 16
                      ),
                      itemBuilder: (context, index) {
                        return ItemMessageTemplate(
                          colorBackground: Colors.redAccent,
                          colorBorder: Colors.black,
                          contentMessage: list[index],
                          onClickItem: (text, valueInt) {
                            print('$index: $text');
                          },
                        );
                      },
                    ),
                  ),

                  // Padding(
                  //   padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  //   child: Row(
                  //       children: [
                  //         SizedBox(
                  //           width: 20,
                  //         ),
                  //         Expanded(
                  //           flex: 1,
                  //           child: Center(
                  //             child: Container(
                  //               height: 140,
                  //                 decoration: BoxDecoration(
                  //                     color: Colors.pink[400],
                  //                     borderRadius: BorderRadius.circular(50/2),
                  //                     border: Border.all(color: Colors.pink[800], width: 2)
                  //                 ),
                  //                 child: FlatButton(
                  //                     onPressed: (){
                  //                       onItemClicked(text1);
                  //                     },
                  //                     child: Center(child: Text(text1, style: TextStyle(color: Colors.white),)))),
                  //           ),
                  //         ),
                  //         SizedBox(width: 20,),
                  //         Expanded(
                  //           child: Center(
                  //             child: Container(
                  //                 height: 140,
                  //                 decoration: BoxDecoration(
                  //                     color: Colors.blue[400],
                  //                     borderRadius: BorderRadius.circular(50/2),
                  //                     border: Border.all(color: Colors.blue[800], width: 2)
                  //                 ),
                  //                 child: FlatButton(
                  //                     onPressed: (){
                  //                       onItemClicked(text2);
                  //                     },
                  //                     child: Center(child: Text(text2, style: TextStyle(color: Colors.white),)))),
                  //           ),
                  //         ),
                  //         SizedBox(width: 20,),
                  //         ],
                  //
                  //   ),
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  //   child: Row(
                  //     children: [
                  //       SizedBox(
                  //         width: 20,
                  //       ),
                  //       Expanded(
                  //         flex: 1,
                  //         child: Center(
                  //           child: Container(
                  //               height: 140,
                  //               decoration: BoxDecoration(
                  //                   color: Colors.green[400],
                  //                   borderRadius: BorderRadius.circular(50/2),
                  //                   border: Border.all(color: Colors.green[800], width: 2)
                  //               ),
                  //               child: FlatButton(
                  //                   onPressed: (){
                  //                     onItemClicked(text3);
                  //                   },
                  //                   child: Center(child: Text(text3, style: TextStyle(color: Colors.white),)))),
                  //         ),
                  //       ),
                  //       SizedBox(width: 20,),
                  //       Expanded(
                  //         child: Center(
                  //           child: Container(
                  //               height: 140,
                  //               decoration: BoxDecoration(
                  //                   color: Colors.purple[400],
                  //                   borderRadius: BorderRadius.circular(50/2),
                  //                   border: Border.all(color: Colors.purple[800], width: 2)
                  //               ),
                  //               child: FlatButton(
                  //                   onPressed: (){
                  //                     onItemClicked(text4);
                  //                   },
                  //                   child: Center(child: Text(text4, style: TextStyle(color: Colors.white),)))),
                  //         ),
                  //       ),
                  //       SizedBox(width: 20,),
                  //     ],
                  //
                  //   ),
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  //   child: Row(
                  //     children: [
                  //       SizedBox(
                  //         width: 20,
                  //       ),
                  //       Expanded(
                  //         flex: 1,
                  //         child: Center(
                  //           child: Container(
                  //               height: 140,
                  //               decoration: BoxDecoration(
                  //                   color: Colors.orange[400],
                  //                   borderRadius: BorderRadius.circular(50/2),
                  //                   border: Border.all(color: Colors.orange[800], width: 2)
                  //               ),
                  //               child: FlatButton(
                  //                   onPressed: (){
                  //                     onItemClicked(text5);
                  //                   },
                  //                   child: Center(child: Text(text5, style: TextStyle(color: Colors.white),)))),
                  //         ),
                  //       ),
                  //       SizedBox(width: 20,),
                  //       Expanded(
                  //         child: Center(
                  //           child: Container(
                  //               height: 140,
                  //               decoration: BoxDecoration(
                  //                   color: Colors.teal[400],
                  //                   borderRadius: BorderRadius.circular(50/2),
                  //                   border: Border.all(color: Colors.teal[800], width: 2)
                  //               ),
                  //               child: FlatButton(
                  //                   onPressed: (){
                  //                     onItemClicked(text6);
                  //                   },
                  //                   child: Center(child: Text(text6, style: TextStyle(color: Colors.white),)))),
                  //         ),
                  //       ),
                  //       SizedBox(width: 20,),
                  //     ],
                  //
                  //   ),
                  // ),
            ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              setState(() {
                list.add("abc");
              });
            },
          ),
        )
    );
  }

  onGetBoyClicked() {
    final String username = _userController.text;

  }

  onItemClicked(String req){
    print("girl $req");

    FirebaseFirestore.instance
        .collection('notification').doc('data').set({'message': req});
        // .add({'message': req});
    return;
  }
}
