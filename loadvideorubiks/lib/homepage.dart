import 'package:flutter/material.dart';
import 'package:loadvideorubiks/loadassets.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final tab = new TabBar(tabs: <Tab>[
    new Tab(icon: new Icon(Icons.arrow_forward)),
    new Tab(icon: new Icon(Icons.arrow_downward)),
    new Tab(icon: new Icon(Icons.arrow_back)),
  ]);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: AppBar(
          title: Center(child: Text('RubiksVideo')),
          bottom: TabBar(
            tabs: [
              Tab(text: 'Assets',),
              Tab(text: 'Local',),
              Tab(text: 'Internet',)
            ],
          ),
        ),
        body: TabBarView(
          children: [
            // LoadFromAssets(),
            Center(child: LoadAssets(),),
            Center(child: Text('1'),),
            Center(child: Text('2'),),
          ],
        ),
      ),

    );
  }
}
